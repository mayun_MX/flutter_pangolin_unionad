import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_callback.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_stream.dart';
import 'package:flutter_pangolin_unionad/flutter_pangonlin_statecode.dart';
import 'draw_feed_page.dart';
import 'native_express_page.dart';
import 'splash_page.dart';
import 'dart:async';
import 'package:get/get.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_unionad.dart';

import 'banner_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  var _init = false.obs;
  var _version = ''.obs;
  StreamSubscription? _adViewStream;

  void _initAdStream() {
    _adViewStream = FlutterPangolinStream.registerAdStream(
      flutterUnionadNewInteractionCallBack:
          FlutterPangolinUnionadNewInteractionCallBack(
        onShow: () {
          print("新模板渲染插屏广告显示");
        },
        onSkip: () {
          print("新模板渲染插屏广告跳过");
        },
        onFail: (error) {
          print("新模板渲染插屏广告错误$error");
        },
        onClick: () {
          print("新模板渲染插屏广告点击");
        },
        onClose: () {
          print("新模板渲染插屏广告关闭");
        },
        onReady: () async {
          print("新模板渲染插屏广告预加载准备就绪");
          await FlutterPangolinUnionad.showNewInteractionVideoAd();
        },
        onUnReady: () {
          print("新模板渲染插屏广告预加载未准备就绪");
        },
      ),
      flutterPangolinUnionadRewardCallBack:
          FlutterPangolinUnionadRewardCallBack(
        onShow: () {
          print("激励广告显示");
        },
        onClick: () {
          print("激励广告点击");
        },
        onClose: () {
          print("激励广告关闭");
        },
        onSkip: () {
          print("激励广告跳过");
        },
        onReady: () async {
          print("激励广告预加载准备就绪");
          await FlutterPangolinUnionad.showRewardVideoAd();
        },
        onUnReady: () {
          print("激励广告预加载未准备就绪");
        },
        onFail: (error) {
          print("激励广告失败$error");
        },
        onVerify: (rewardVerify, rewardAmount, rewardName) {
          print(
              "激励广告奖励  结果：$rewardVerify  数量： $rewardAmount  奖励内容： $rewardName");
        },
      ),
    );
  }

  void _initRegister() async {
    _init.value = await FlutterPangolinUnionad.registerPlugin(
        androidAppId: "5224924",
        //穿山甲广告 Android appid 必填
        iosAppId: "5224924",
        //穿山甲广告 ios appid 必填
        useTextureView: true,
        //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView 选填
        appName: "日历工具",
        //appname 必填
        allowShowNotify: true,
        //是否允许sdk展示通知栏提示 选填
        allowShowPageWhenScreenLock: true,
        //是否在锁屏场景支持展示广告落地页 选填
        debug: true,
        //是否显示debug日志
        supportMultiProcess: true,
        //是否支持多进程，true支持 选填
        directDownloadNetworkType: [
          FlutterPangolinUnionadNetStateCode.NETWORK_STATE_2G,
          FlutterPangolinUnionadNetStateCode.NETWORK_STATE_3G,
          FlutterPangolinUnionadNetStateCode.NETWORK_STATE_4G,
          FlutterPangolinUnionadNetStateCode.NETWORK_STATE_WIFI
        ]); //允许直接下载的网络状态集合 选填
    _version.value = await FlutterPangolinUnionad.sdkVersion();
  }

  void _privacy() async {
    if (Platform.isAndroid) {}
  }

  @override
  Widget build(BuildContext context) {
    _privacy();
    _initRegister();
    _initAdStream();

    return Obx(
      () => _init.value == false
          ? GetMaterialApp(
              home: Scaffold(
                body: Center(
                  child: Text("正在进行穿山甲sdk初始化..."),
                ),
              ),
            )
          : GetMaterialApp(
              debugShowCheckedModeBanner: false,
              home: Scaffold(
                appBar: AppBar(
                  title: const Text('广告接入demo'),
                ),
                body: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  reverse: false,
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: 50,
                        child: Text(
                            "广告初始化结果>>>>>> ${_init.value != false ? "success" : "fail"}"),
                      ),
                      Container(
                        alignment: Alignment.center,
                        height: 50,
                        child: Text("广告SDK版本号>>>>>> v${_version.value}"),
                      ),
                      //请求权限
                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('请求权限,针对iOS14（请用真机）'),
                        onPressed: () async {
                          FlutterPangolinUnionad.requestPermissionIfNecessary(
                            callback: FlutterPangolinUnionadPermissionCallBack(
                              notDetermined: () {
                                print("权限未确定");
                              },
                              restricted: () {
                                print("权限限制");
                              },
                              denied: () {
                                print("权限拒绝");
                              },
                              authorized: () {
                                print("权限同意");
                              },
                            ),
                          );
                        },
                      ),
                      // banner广告
                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('banner广告'),
                        onPressed: () {
                          Get.to(() => BannerPage());
                        },
                      ),
                      //开屏广告
                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('开屏广告'),
                        onPressed: () {
                          Get.to(() => SplashPage());
                        },
                      ),
                      // 个性化模板信息流广告
                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('信息流广告'),
                        onPressed: () {
                          Get.to(() => NativeExpressAdPage());
                        },
                      ),
                      //插屏广告

                      //激励视频
                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('激励视频'),
                        onPressed: () {
                          FlutterPangolinUnionad.loadRewardVideoAd(
                            mIsExpress: true,
                            //是否个性化 选填
                            androidAppId: "946863833",
                            //Android 激励视频广告id  必填
                            iosAppId: "946863833",
                            //ios 激励视频广告id  必填
                            supportDeepLink: true,
                            //是否支持 DeepLink 选填
                            rewardName: "100金币",
                            //奖励名称 选填
                            rewardAmount: 100,
                            //奖励数量 选填
                            userID: "123",
                            //用户id 选填
                            orientation:
                                FlutterPangolinUnionadOrientation.VERTICAL,
                            //视屏方向 选填
                            mediaExtra: null, //扩展参数 选填
                          );
                        },
                      ),
                      // 个性化模板draw广告
                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('draw视频广告'),
                        onPressed: () {
                          Get.to(() => DrawFeedPage());
                        },
                      ),

                      MaterialButton(
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: new Text('新模板渲染插屏广告'),
                        onPressed: () {
                          //新模版支持全屏和插屏，由广告ID类型决定：如全屏：946863837 插屏：946863844
                          FlutterPangolinUnionad.preloadNewInteractionVideoAd(
                            androidAppId: "946863844", //android 全屏广告id 必填
                            iosAppId: "946863844", //ios 全屏广告id 必填
                            supportDeepLink: true, //是否支持 DeepLink 选填
                            orientation: FlutterPangolinUnionadOrientation
                                .VERTICAL, //视屏方向 选填
                          );
                        },
                      ),
                    ],
                  ),
                ),
              )),
    );
  }
}
