import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_callback.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_unionad.dart';
import 'package:get/get.dart';

class SplashPage extends StatelessWidget {
  var _offstage = true.obs;
  SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          children: [
            Offstage(
              offstage: _offstage.value,
              child: FlutterPangolinUnionad.loadSplashViewAd(
                //是否使用个性化模版  设定widget宽高
                mIsExpress: true,
                //android 开屏广告广告id 必填
                androidAppId: "887588578",
                //ios 开屏广告广告id 必填
                iosAppId: "887588578",
                //是否支持 DeepLink 选填
                supportDeepLink: true,
                // 期望view 宽度 dp 选填 mIsExpress=true必填
                expressWidth: MediaQuery.of(context).size.width,
                //期望view高度 dp 选填 mIsExpress=true必填
                expressHeight: MediaQuery.of(context).size.height,
                callBack: FlutterPangolinUnionadSplashCallBack(
                  onShow: () {
                    print("开屏广告显示");
                    _offstage.value = false;
                  },
                  onClick: () {
                    print("开屏广告点击");
                    Get.back();
                  },
                  onFail: (error) {
                    print("开屏广告失败 $error");
                    //加载失败，设定1秒进入app
                    Timer(Duration(seconds: 1), () {
                      Get.back();
                    });
                  },
                  onFinish: () {
                    print("开屏广告倒计时结束");
                    Get.back();
                  },
                  onSkip: () {
                    print("开屏广告跳过");
                    Get.back();
                  },
                  onTimeOut: () {
                    print("开屏广告超时");
                  },
                ),
              ),
            ),

            /// app 原来闪屏图
            Expanded(
              child: Container(
                color: Colors.black,
                alignment: Alignment.center,
                child: Text(
                  "App Name",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
