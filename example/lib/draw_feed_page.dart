import 'package:flutter/material.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_callback.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_unionad.dart';
import 'package:get/get.dart';

class DrawFeedPage extends StatelessWidget {
  var _offstage = true.obs;
  DrawFeedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        centerTitle: true,
        title: Text(
          "draw视频广告",
        ),
      ),
      body: PageView.custom(
        scrollDirection: Axis.horizontal, //方向
        childrenDelegate: SliverChildBuilderDelegate(
          (context, index) {
            return Obx(
              () => Container(
                child: Offstage(
                  offstage: _offstage.value,
                  child: FlutterPangolinUnionad.drawFeedAdView(
                    androidAppId: "946863823",
                    // Android draw视屏广告id 必填
                    iosAppId: "946863823",
                    //ios draw视屏广告id 必填
                    supportDeepLink: true,
                    //是否支持 DeepLink 选填
                    expressWidth: MediaQuery.of(context).size.width,
                    // 期望view 宽度 dp 必填
                    expressHeight: 0,
                    //期望view高度 dp 必填
                    callBack: FlutterPangolinUnionadDrawFeedCallBack(
                      onShow: () {
                        print("draw广告显示");
                        _offstage.value = false;
                      },
                      onFail: (error) {
                        print("draw广告加载失败 $error");
                      },
                      onClick: () {
                        print("draw广告点击");
                      },
                      onDislike: (message) {
                        print("draw点击不喜欢 $message");
                      },
                      onVideoPlay: () {
                        print("draw视频播放");
                      },
                      onVideoPause: () {
                        print("draw视频暂停");
                      },
                      onVideoStop: () {
                        print("draw视频结束");
                      },
                    ),
                  ),
                ),
              ),
            );
          },
          childCount: 3,
        ),
      ),
    );
  }
}
