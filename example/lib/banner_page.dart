import 'package:flutter/material.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_callback.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_unionad.dart';

class BannerPage extends StatelessWidget {
  const BannerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        centerTitle: true,
        title: Text('banner广告'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        reverse: false,
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            FlutterPangolinUnionad.bannerAdView(
              //andrrid banner广告id 必填
              androidAppId: "946863433",
              //ios banner广告id 必填
              iosAppId: "946863433",
              //是否使用个性化模版
              mIsExpress: true,
              //是否支持 DeepLink 选填
              supportDeepLink: true,
              //一次请求广告数量 大于1小于3 必填
              expressAdNum: 2,
              //轮播间隔事件 30-120秒  选填
              expressTime: 30,
              // 期望view 宽度 dp 必填
              expressWidth: MediaQuery.of(context).size.width,
              //期望view高度 dp 必填
              expressHeight: 120.5,
              //广告事件回调 选填
              callBack: FlutterPangolinUnionadBannerCallBack(
                onShow: () {
                  print("banner广告加载完毕");
                },
                onDislike: (message) {
                  print("banner不感兴趣$message");
                },
                onFail: (error) {
                  print("banner广告加载失败$error");
                },
                onClick: () {
                  print("banner广告点击");
                },
              ),
            ),
            SizedBox(
              height: 320,
            ),
            FlutterPangolinUnionad.bannerAdView(
              //andrrid banner广告id 必填
              androidAppId: "946863447",
              //ios banner广告id 必填
              iosAppId: "946863447",
              //是否使用个性化模版
              mIsExpress: true,
              //是否支持 DeepLink 选填
              supportDeepLink: true,
              //一次请求广告数量 大于1小于3 必填
              expressAdNum: 2,
              //轮播间隔事件 30-120秒  选填
              expressTime: 30,
              // 期望view 宽度 dp 必填
              expressWidth: MediaQuery.of(context).size.width,
              //期望view高度 dp 必填
              expressHeight: 120.5,
              //广告事件回调 选填
              callBack: FlutterPangolinUnionadBannerCallBack(
                onShow: () {
                  print("banner广告加载完毕");
                },
                onDislike: (message) {
                  print("banner不感兴趣$message");
                },
                onFail: (error) {
                  print("banner广告加载失败$error");
                },
                onClick: () {
                  print("banner广告点击");
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
