import 'package:flutter/material.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_callback.dart';
import 'package:flutter_pangolin_unionad/flutter_pangolin_unionad.dart';

class NativeExpressAdPage extends StatelessWidget {
  const NativeExpressAdPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        centerTitle: true,
        title: Text(
          "个性化模板信息流广告",
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        reverse: false,
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            //个性化模板信息流广告
            FlutterPangolinUnionad.nativeViewAd(
              androidAppId: "946863786",
              //android 信息流广告id 必填
              iosAppId: "946863786",
              //ios banner广告id 必填
              supportDeepLink: true,
              //是否支持 DeepLink 选填
              expressWidth: 375.5,
              // 期望view 宽度 dp 必填
              expressHeight: 275.5,
              //期望view高度 dp 必填
              expressNum: 2,
              //一次请求广告数量 大于1小于3 必填
              mIsExpress: true,

              callBack: FlutterPangolinUnionadNativeCallBack(
                onShow: () {
                  print("信息流广告显示");
                },
                onFail: (error) {
                  print("信息流广告失败 $error");
                },
                onDislike: (message) {
                  print("信息流广告不感兴趣 $message");
                },
                onClick: () {
                  print("信息流广告点击");
                },
              ),
            ), //个性化模板信息流广告
            FlutterPangolinUnionad.nativeViewAd(
              androidAppId: "946863793",
              iosAppId: "946863793",
              supportDeepLink: true,
              expressWidth: 375.5,
              expressHeight: 284.5,
              expressNum: 3,
            ), //个性化模板信息流广告
            FlutterPangolinUnionad.nativeViewAd(
              androidAppId: "946863800",
              iosAppId: "946863800",
              supportDeepLink: true,
              expressWidth: 270,
              expressHeight: 400,
              expressNum: 3,
            ), //个性化模板信息流广告
            FlutterPangolinUnionad.nativeViewAd(
              androidAppId: "946863803",
              iosAppId: "946863803",
              supportDeepLink: true,
              expressWidth: 270,
              expressHeight: 480,
              expressNum: 3,
            ),
          ],
        ),
      ),
    );
  }
}
